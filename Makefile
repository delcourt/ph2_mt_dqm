
_DEPS = TreeReader.h Histogrammer.h BasicPlots.h
_OBJ = TreeReader.o Histogrammer.o BasicPlots.o
_PLUGINS = test.out plotter.out analyser.out

IDIR = ./include
LDIR = ./lib
ODIR = ./src
PDIR = ./plugins
BDIR = ./bin

PLUGINS = $(patsubst %,$(BDIR)/%,$(_PLUGINS))


CFLAGS = -I$(IDIR) -I./  -L$(ROOTSYS)/lib
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))
CC = g++
COpt = -Wall -Wextra  `root-config --cflags --libs` -O3

all:
	@echo "############# COMPILING ALL #############"
	@echo "###### COMPILING DEPENDENCIES..."
	@make  -j4 $(OBJ)
	@echo "###### COMPILING $(PLUGINS)..."
	@make -j8 $(PLUGINS)

%.o: %.cc
	$(CC) -c -o $@ $< $(CFLAGS) $(ROOTLIBS) -L$(ROOTSYS)/lib $(COpt) 


bin/%.out: plugins/%.cc $(OBJ)
	$(CC)  -o $@ $^ $(CFLAGS) $(ROOTLIBS) -L$(ROOTSYS)/lib $(COpt)


.PHONY: clean

clean:
	rm -rf $(ODIR)/*.o hellomake src/Dict.cc include/Dict.h bin/* Dict_rdict.pcm plugins/*.out

  
