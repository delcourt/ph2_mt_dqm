# Phase 2 Magnet Test Data Quality Monitoring
## Introduction

This package was developped to offer a simple and automatic DQM for the 2021 CMS 2S magnet test. It was designed to run on SLink files unpacked using the external [dummy_unpacker](https://gitlab.cern.ch/delcourt/Ph2_dummy_unpacker). From these simple flat ntuples, this package contains a C++ code designed to produce histograms and graphs and store them in root output files. Finally, a configurable python library produces png and pdf's from these files and builds web-pages for easy monitoring.

## Histogrammer

The first software of this package is the C++ Histogrammer. In order to use this program two seperate objects are needed :
* The tree reader (defined in TreeReader.h) that provides an easy interface to loop on the ntuple
* The histogrammer (defined in Histogrammer.h) that stores functions to be applied on the data and produces the corresponding histograms after looping on the data.

### Tree reader

Simple ntuple interface. Relevant methods : 
* `TreeReader(string file_name)` : Constuctor. Requires the ntuple input file as argument.
* `int getEvent(int event_number = -1)` : Loads tree entry #`event_number` to the object's `event e` attribute. If no event number is given (or -1), the next entry in the tree will be read. Returns 0 if the event can't be read (or at end of file).
* `int getEntries()` : Return the number of entries in the tree.

If required, additional producers could be added to provide reconstructed clusters or stubs.

The `event` data-format is the following :
```c
    vector <int>   *    hit0;          //Hits on the even strips
    vector <int>   *    hit1;          //Hits on the odd strips
    vector <int>   *    stub;          //Stub position (in half strip units)
    vector <int>   *    bendCode;      //Stub bend code
    vector <float> *    bend;          //Empty
    vector <int>   *    stubSim;       //Empty
    vector <int>   *    bendCodeSim;   //Empty
    vector <int>   *    bendSim;       //Empty
    uint32_t            L1A;           //Event L1A as given by Ph2_ACF
    vector <uint16_t> * CbcL1A;        //Individual Cbc L1A counter
    vector <uint16_t> * Pipeline;      //CBC Pipeline adresses of the event
    vector <uint16_t> * Error;         //CBC error bits
    uint8_t             TDC;            //Empty
    uint32_t            Timestamp;      //Event timestamp, as given by Ph2_ACF
```

TODO : The VCTH values are stored in the condition data, but not in this event. This should be added...

### Histogrammer

The Histogrammer class allows for easy plotting on event-based root files. The important methods are the following :
* `Add_1d_hist(function, hist_title, nbinx = 1024, x0 = 0, x1 = 1024)` : Add a 1 dimentional histogram that will be filled using the given function. This function can either be taking an event and returning a float (that will be filled in the histogram), or taking both an event and a histogram, filling it itself, and returning nothing. The `string hist_title` parameter will be the name of the histogram and should be unique. The last three optional parameters define the binning of the histogram.
* `Add_2d_hist(function, hist_title, nbinx = 1024, x0 = 0, x1 = 1024,  nbiny = 1024, y0 = 0, y1 = 1024)` : Similar to the previous method, but for 2D histograms. `function` either takes an event and a hist as input (returning nothing), or only takes an event but returns an `pair<float,float>` that will be filled in the histogram.
* `Add_time_hist(function, hist_title, average=true)` : Creates a time-dependent graph. The `function` takes an event as parameter and returns a float. The `average` parameters defines if the value for each time point should be divided by the number of events for that particular time point.
* `Fill_Hists(const event & e)` : Should be ran on an event that is to be added to the histograms. Obviously, has to come AFTER the histogram definitions.
* `Save_Hists(string fName)` : Stores the hists to file.
* `Process_ntuple(string ntuple_name, string plots_name)`: Loops on the `ntuple_name` file and stores histograms to the `plots_name` file.


### BasicPlots

Defines a series of basic plots that can be added to the DQM file. You can use them through the following functions :
* `void add_header_plots(Histogrammer * hm)` : Adding DQM relative to header information.
* `void add_beam_profile(Histogrammer * hm)` : Adding DQM relative to basic hit and cluster information.
* `void add_all_basic_plots(Histogrammer * hm)` : Add all plots defined above.


## Plotter & web page builder

With the heavy lifting done, we can move from C++ to python. Two classes are defined : 
* `plotter` that creates the png/pdf files from the `.root` file and a `.json` configuration
* `website_builder` that produces a web-page to display these files (with the template stolen from Ph2_ACF)


### Plotter

The usage of the plotter class is the following :

```python
from plotter import plotter
p = plotter("conf.json")
p.plot("input_file.root","output_dir")
```
Where `conf.json` is the configuration file (see bellow), "input_file.root" is a DQM file produced with the package described above and `output_dir` is where you want to store the plots. If this directory doesn't exist, it will be created.

The configuration `.json` file should contain a dictionary with the following entries :
```python
{
 "res_small"              : [x_res, y_res],  #resolution of thumbnails
 "res_big"                : [x_res, y_res],  #resolution of plots
 "default_options"        : options,         #default options applied to all
 "TH1_default_options"    : options,         #options applied to all TH1 plots
 "TH2_default_options"    : options,         #options applied to all TH2 plots
 "TGraph_default_options" : options,         #options applied to all TH2 plots
 "option_list"            :                  
        {
        "plot_name"  : options,              #Options applied only to a given plot
        "other_name" : options
        }

}
```
An option is a dictionary with the following optional key/value meanings :
``` python
    "rangeX"   : [x0, x1]
    "rangeY"   : [y0, y1]                   #Defines x,y,z axis ranges
    "rangeZ"   : [z0, z1]
    "scaleX"   : "linear","log" or "both"
    "scaleY"   : "linear","log" or "both"   #Defines if axis should be
    "scale2"   : "linear","log" or "both"   #linear, log, or both
    "norm"     : 0 or 1                     #Normalise histogram
    "rebinX"   : N
    "rebinY"   : N                          #Rebin by a factor N
    "rebinZ"   : N
    "style"    : "value"                    #given to obj.Draw()
    "marker"   : [color, style, size]       # Set marker properties (int,int,int)
    "line"     : [color, style, size]       # Set line properties   (int,int,int)
    "showStats": True or False              # Keep the ugly stats box or not
```

An option is applied only if implemented somewhere and if an option is defined multiple times, the most specific object takes precedence (eg : option_list["my_plot"] > "TH1_default_options" > "default_options").

### Web builder

This last class will look at plots printed in a given directory and produce a [Ph2_ACF DQM style](https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF/-/tree/Dev/RootWeb) web-page accordingly.

```python
from website_builder import website_builder
WB = website_builder("directory")
WB.generate()
```

The stolen Ph2_ACF style should be located in the parent directory of the location of this web-page.

### Supervisor

In order to run all this automatically, a small supervisor script is provided. The usage is the following :

```python
python3 supervisor.py --help                
python3 supervisor.py                       #Automatically process new files
python3 supervisor.py --stage stage         #Process only starting at a given stage (unpack [default] | dqm | plot)
python3 supervisor.py --reprocess           #Reprocess everything
```

You have to adapt the beginning of this script for your given installation.
