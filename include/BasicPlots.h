#pragma once
#include "Histogrammer.h"

void add_all_basic_plots(Histogrammer * hm);
void add_header_plots(Histogrammer * hm);
void add_beam_profile(Histogrammer * hm);


