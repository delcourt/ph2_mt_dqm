#pragma once
#include "TH1.h"
#include "TH2.h"
#include "TreeReader.h"
#include <iostream>

class Histogrammer{
    private:
    std::vector<std::pair <TH1 *,float (*)(const event &)>> _1d_hists;
    std::vector<std::pair <TH2 *, std::pair<float,float> (*) (const event & e)>> _2d_hists;
    std::vector<std::pair <TH1 *, void (*)(const event &, TH1 * hist)>> _1d_hists_manual;
    std::vector<std::pair <TH2 *, void (*) (const event & e, TH2 * hist)>> _2d_hists_manual;
    std::vector<std::pair <std::map <uint32_t, std::pair<float, float>> , std::tuple < std::vector<std::string>, float (*) (const event & e), bool>>> _time_hists;
    
    public:
    Histogrammer();
    ~Histogrammer();

    void Add_1d_hist(float function (const event & e), std::string hist_title, int nbinx = 2048, int x0 = 0, int x1 = 2048);
    void Add_1d_hist(float function (const event & e), std::string hist_title, std::string legendX, std::string legendY, int nbinx = 2048, int x0 = 0, int x1 = 2048);

    void Add_2d_hist(std::pair<float,float> function (const event & e), std::string hist_title,  int nbinx = 2048, int x0 = 0, int x1 = 2048,  int nbiny = 2048, int y0 = 0, int y1 = 2048);
    void Add_2d_hist(std::pair<float,float> function (const event & e), std::string hist_title, std::string legendX, std::string legendY, int nbinx = 2048, int x0 = 0, int x1 = 2048,  int nbiny = 2048, int y0 = 0, int y1 = 2048);

    void Add_1d_hist(void function (const event & e, TH1 * hist), std::string hist_title, std::string legendX, std::string legendY, int nbinx = 2048, int x0 = 0, int x1 = 2048);
    void Add_1d_hist(void function (const event & e, TH1 * hist), std::string hist_title, int nbinx = 2048, int x0 = 0, int x1 = 2048);

    void Add_2d_hist(void function (const event & e, TH2 * hist), std::string hist_title, std::string legendX, std::string legendY, int nbinx = 2048, int x0 = 0, int x1 = 2048,  int nbiny = 2048, int y0 = 0, int y1 = 2048);
    void Add_2d_hist(void function (const event & e, TH2 * hist), std::string hist_title,  int nbinx = 2048, int x0 = 0, int x1 = 2048,  int nbiny = 2048, int y0 = 0, int y1 = 2048);

    void Add_time_hist(float function (const event & e), std::string hist_title, std::string legendX, std::string legendY,bool average=true);
    void Add_time_hist(float function (const event & e), std::string hist_title, bool average=true);

    void Fill_Hists(const event & e);
    void Save_Hists(std::string fName);
    void Process_ntuple(std::string ntuple_name, std::string plots_name);
};
