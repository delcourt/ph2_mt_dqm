#pragma once
#include <iostream>
#include <vector>

#include "TTree.h"
#include "TFile.h"


struct event{
    event();
    ~event();
    std::vector <int>   *    hit0;
    std::vector <int>   *    hit1;
    std::vector <int>   *    stub;
    std::vector <int>   *    bendCode;
    std::vector <float> *    bend;
    std::vector <int>   *    stubSim;
    std::vector <int>   *    bendCodeSim;
    std::vector <int>   *    bendSim;
    uint32_t                L1A;
    std::vector <uint16_t> * CbcL1A;
    std::vector <uint16_t> * Pipeline;
    std::vector <uint16_t> * Error;
    uint8_t                 TDC;
    uint16_t delay_and_group;
    uint16_t vcth;
    uint32_t                Timestamp;
};

class TreeReader{
    private:
    TTree * _tt;
    TFile * _ff;
    int     event_counter; 
    int     n_entries;

    public:

    TreeReader(std::string file_name);
    ~TreeReader();

    int     getEvent(int event_number = -1);
    int     getEntries();
    event   e;
};
