#include "TreeReader.h"
#include "TH1I.h"
#include <map>
#include <iostream>

using namespace std;
int main(int argc, char ** argv){
    assert(argc == 2);

    TreeReader tree_reader(argv[1]);

    map <int,TH1I*> histos;
    for (int i = 0; i<8; i++){
	    string name = "tp_group_"+to_string(i);
	    histos[i]=new TH1I(name.c_str(), name.c_str(), 4064,0,4064);
    }


    map <int,int> delay_counter;
    int i = 0;
    while (tree_reader.getEvent()){
	if((i%10000) == 0){
		cout<<i<<"/"<<tree_reader.getEntries()<<endl;
	}
	int tp = tree_reader.e.delay_and_group & 0x7;
        delay_counter[tp] ++;
	for (auto h: *(tree_reader.e.hit0))
		histos[tp]->Fill(2*h);
	for (auto h: *(tree_reader.e.hit1))
		histos[tp]->Fill(2*h+1);
	i++;
    }

   string output_file_name = string("out_")+string(argv[1]).substr(string(argv[1]).find_last_of("/\\")+1);

   TFile * output_file = new TFile(output_file_name.c_str(),"RECREATE");

   for (auto histo_key : histos)
       histo_key.second->Write();
   output_file->Close();

}
