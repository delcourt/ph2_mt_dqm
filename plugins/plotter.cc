#include "TreeReader.h"
#include "Histogrammer.h"
#include "BasicPlots.h"

#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    assert(argc == 3);
    
    Histogrammer h;
    add_all_basic_plots(&h);
    h.Process_ntuple(argv[1],argv[2]);
}