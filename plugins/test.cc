#include "TreeReader.h"
#include "Histogrammer.h"
#include "BasicPlots.h"

#include <iostream>
using namespace std;

float test_fn(const event & e){return e.hit0->size();}
pair<float,float> test_fn_4(const event & e){ return pair<float,float> (e.hit0->size(),e.hit1->size());}

void test_fn_5(const event & e, TH1 * h){
    for (auto hit : *(e.hit0))
        h->Fill(hit);
}

void test_fn_6(const event & e, TH2 * h){
    if (e.hit0->size() > 50 && e.hit1->size() > 50)
        return;
    for (auto hit0 : *(e.hit0)){
        for(auto hit1: *(e.hit1)){
            h->Fill(hit0,hit1);
        }
    }
}

int main(){
    TreeReader r("test.root");
    std::cout<<"Started"<<r.getEntries()<<std::endl;
    int n_non_empty = 0;
    
    Histogrammer h;
    add_all_basic_plots(&h);

    h.Add_1d_hist(test_fn,"Number of hits S0");
    h.Add_time_hist(test_fn,"Avg number of hits vs time");
    h.Add_time_hist(test_fn,"Abs number of hits vs time",false);
    h.Add_2d_hist(test_fn_4,"Number of hits S0vS1");
    h.Add_1d_hist(test_fn_5,"hit profile S0");
    h.Add_2d_hist(test_fn_6,"hit profile S0 v S1");

    int event = 0;
    while(r.getEvent()){
        h.Fill_Hists(r.e);
        if (event%1000 == 0)
            cout<<event<<endl;
        event++;
    }

    h.Save_Hists("histograms.root");
    std::cout<<"Done !"<<n_non_empty<<std::endl;
}