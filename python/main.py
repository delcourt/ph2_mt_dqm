from website_builder import website_builder
from plotter import plotter


if __name__ == "__main__":
    print("Debug !")
    p = plotter("conf.json")
    p.plot("../histograms.root","../web/test" )

    web = website_builder("../web/test")
    web.generate()
