from ROOT import TFile, TCanvas, TH1, TH2, TGraph,gROOT, gStyle, kWarning
import ROOT
ROOT.gErrorIgnoreLevel = kWarning
import os, json

def check_create(directory):
    if not os.path.isdir(directory):
        os.mkdir(directory)

def merge_dict(dic_1, dic_2):
    new_dic = {}
    for key in dic_1:
        new_dic[key] = dic_1[key]
    for key in dic_2:
        new_dic[key] = dic_2[key]
    return new_dic

def clean_name(input_name):
    for cc in " ,.;:/=~+*\\@":
        input_name = input_name.replace(cc,"_")
    return input_name

def check_bits(scale_word,logX,logY,logZ):
    return scale_word & 1<<(4+logX) and scale_word & 1<<(2+logY) and scale_word & 1<<(logZ)

def add_logs(filename,logX,logY,logZ):
    return filename+logX*"_logX"+logY*"_logY"+logZ*"_logZ"

scale_code = {
    "none"   : 0,
    "linear" : 1,
    "log"    : 2,
    "both"   : 3
}

class plotter:
    def __init__(self,config_file, batch = True):
        self.conf = json.load(open(config_file,"r"))
        if batch:
            gROOT.SetBatch(True)
    
    def plot(self, input_file, output_dir):
        check_create(output_dir)
        check_create(output_dir+"/small")
        check_create(output_dir+"/full")

        ff = TFile(input_file)
        #dd = ff.Get("histos")
        dd = ff
        print(dd)
        n_plot = 0
        n_plots = len(dd.GetListOfKeys())
        for plot_name in dd.GetListOfKeys():
            n_plot+=1
            print(f"Processing plot {n_plot}/{n_plots}")

            plot = dd.Get(plot_name.GetName())
            options = self.conf["default_options"]
            if isinstance(plot,TH2):
                options = merge_dict(options,self.conf["TH2_default_options"])
            elif isinstance(plot,TH1):
                options = merge_dict(options,self.conf["TH1_default_options"])
            elif isinstance(plot,TGraph):
                options = merge_dict(options,self.conf["TGraph_default_options"])
            else:
                print(f"I can't plot {plot}")
                return
            if plot.GetName() in self.conf["option_list"].keys():
                options = merge_dict(options,self.conf["option_list"][plot.GetName()])
            else:
                clear_name = plot.GetName()
                for i in range(10):
                    clear_name = clear_name.replace(str(i),"*")
                if clear_name in self.conf["option_list"].keys():
                    options = merge_dict(options,self.conf["option_list"][clear_name])

            self.produce_plot(plot,options,output_dir)

    def produce_plot(self,obj,options,output_dir):
        #Processing options
        #rangeX   : x0, x1
        if "rangeX" in options.keys():
            x0,x1 = options["rangeX"]
            obj.GetXaxis().SetRangeUser(x0,x1)

        if "rangeY" in options.keys():
            y0,y1 = options["rangeY"]
            obj.GetYaxis().SetRangeUser(y0,y1)

        if "rangeZ" in options.keys():
            z0,z1 = options["rangeZ"]
            obj.GetZaxis().SetRangeUser(z0,z1)
        
        if "norm" in options.keys() and options["norm"] == True:
            obj.Scale(1./obj.Integral())

        if "rebinX" in options.keys():
            obj.RebinX(int(options["rebinX"]))
        if "rebinY" in options.keys():
            obj.RebinY(int(options["rebinY"]))

        if "marker" in options.keys():
            color,style,size = options["marker"]
            obj.SetMarkerColor(color)
            obj.SetMarkerStyle(style)
            obj.SetMarkerSize(size)
        if "line" in options.keys():
            color,style,size = options["line"]
            obj.SetLineColor(color)
            obj.SetLineStyle(style)
            obj.SetLineSize(size)
    
        style = options["style"]
        scaleX = "linear" if not "scaleX" in options.keys() else options["scaleX"]
        scaleY = "linear" if not "scaleY" in options.keys() else options["scaleY"]
        scaleZ = "linear" if not "scaleZ" in options.keys() else options["scaleZ"]

        scaleX = scale_code[scaleX]
        scaleY = scale_code[scaleY]
        scaleZ = scale_code[scaleZ]

        scale_word = (scaleX<<4) + (scaleY <<2) + (scaleZ)


        filename = clean_name(obj.GetTitle()) if not "filename" in options.keys() else options["filename"]
        #Printing big to file
        resX, resY = self.conf["res_big"]
        cc = TCanvas("canvas","canvas",resX,resY)
        showStats = True if not "showStats" in options.keys() else options["showStats"]
        gStyle.SetOptStat(showStats)

        for logX in (0,1):
            for logY in (0,1):
                for logZ in (0,1):
                    cc.SetLogx(logX)
                    cc.SetLogy(logY)
                    cc.SetLogz(logZ)
                    if check_bits(scale_word,logX,logY,logZ):                            
                        obj.Draw(style)
                        cc.Print(output_dir+"/full/"+add_logs(filename,logX,logY,logZ)+".png")
                        cc.Print(output_dir+"/full/"+add_logs(filename,logX,logY,logZ)+".pdf")

        del cc
        resX, resY = self.conf["res_small"]
        cc = TCanvas("canvas","canvas",resX,resY)
        for logX in (0,1):
            for logY in (0,1):
                for logZ in (0,1):
                    cc.SetLogx(logX)
                    cc.SetLogy(logY)
                    cc.SetLogz(logZ)
                    if check_bits(scale_word,logX,logY,logZ):
                        obj.Draw(style)
                        cc.Print(output_dir+"/small/"+add_logs(filename,logX,logY,logZ)+".png")
                        


                


        
            

        
