import os, argparse,glob,time, sys, subprocess
from datetime import datetime


### Configuration :

reprocess_time   = 5*60 # Reprocess file if dt < reprocess_time

DATA_DIR         = "/eos/user/d/delcourt/MUONEBT2022/DQM_DATA/"
WORK_DIR         = "/afs/cern.ch/user/d/delcourt/work/MUONE_DQM/"
daq_folder       = DATA_DIR + "DAQ/"
unpacked_folder  = DATA_DIR + "NTUPLE/"
unpack_merged_folder = DATA_DIR + "NTUPLE_MERGED/"
plots_folder     = DATA_DIR + "PLOTS/"

web_folder       = WORK_DIR + "web_page/"
unpacker_cmd     = WORK_DIR + "Ph2_dummy_unpacker/bin/converter.out {file_in} {file_out}"
plotter_cmd      = WORK_DIR + "muone_live_analysis/bin/DQM_muone.out {file_in} {file_out}"

lock_file        = WORK_DIR + ".supervisor.lock"

if os.path.isfile(lock_file):
    print("lock file exists!")
    if time.time() - os.path.getmtime(lock_file) > 10*60:
        print("Lock file was here for more than 10 minutes... Ignoring it")
    else:
        print ("Exiting")
        sys.exit(0)


#Check if style folder is present :
if not os.path.isdir(web_folder+"style/"):
    print("Error ! Missing style folder !")
    if os.path.isdir(WORK_DIR+"ph2_mt_dqm/python/style"):

        print("Run the following command to solve :")
        print(f"cp -r {WORK_DIR}ph2_mt_dqm/python/style {web_folder}")
    else:
        print("Cannot find style. Make sure configuration is correct")
    sys.exit()


def get_ntuple_file(daq_file, run_number):
    return(unpacked_folder+run_number+"/"+daq_file.split("/")[-1].replace(".dat","_ntuple.root"))
def get_plot_file(ntuple_file):
    return(plots_folder+ntuple_file.split("/")[-1].replace("_ntuple.root","_plots.root"))
def get_web_folder(plot_file):
    return(web_folder+plot_file.split("/")[-1].replace(".root","").replace("histos_", "run "))





### Arguments : 

parser = argparse.ArgumentParser(description='Run the magnet test DQM')
parser.add_argument('--stage' , metavar='stage', type=str, help='First stage to run (plot)', choices=['unpack','plot'], default="unpack")
parser.add_argument('--reprocess', action='store_true', help='Reprocess all data')
parser.add_argument('--loop', action='store_true', help='Run in a loop')
args = parser.parse_args()
stage     = args.stage
reprocess = args.reprocess
loop = args.loop

import website_builder,plotter


def unpack(daq_file, run_number):
    cmd = unpacker_cmd.format(file_in=daq_file, file_out=get_ntuple_file(daq_file,run_number))
    print(cmd)
    return(os.system(cmd))

def plot(ntuple_file):
    cmd = plotter_cmd.format(file_in=ntuple_file, file_out=get_plot_file(ntuple_file))
    print(cmd)
    return(os.system(cmd))

def build_web(plot_file):   
    try:
        p = plotter.plotter(WORK_DIR+"/ph2_mt_dqm/python/conf.json")
        p.plot(plot_file,get_web_folder(plot_file))
        WB = website_builder.website_builder(get_web_folder(plot_file),WORK_DIR+"/ph2_mt_dqm/python/index_template.html")
        WB.generate()
    except Exception as e:
        print("Unable to create web page !")
        print(e)

first_pass = True
while first_pass or loop:
    start_time = datetime.now().replace(microsecond=0)
    first_pass = False
    os.system(f"touch {lock_file}")
    if stage == "unpack":
        #Unpacking can be done only one file at a time...
        # --> Choice is:
        # * Last file from last run.
        # * If file already processed, process previous one.

        run_list = subprocess.check_output("ssh daquser@muedaq03 'ls -t /data/muedaq'", shell=True).decode("utf-8").split("\n")
        print(run_list)
        to_process = None
        run_number = ""
        for run in run_list:
            run_number = run
            if not run in os.listdir(unpacked_folder):
                print("Creating run directory")
                os.mkdir(unpacked_folder+"/"+run)
            #Getting file list:
            file_list = subprocess.check_output(f"ssh daquser@muedaq03 'ls -t /data/muedaq/{run}'", shell=True).decode("utf-8").split("\n")
            for file_name in file_list:
                if not file_name.replace(".dat","_ntuple.root") in os.listdir(unpacked_folder+"/"+run):
                    #print(os.listdir(unpacked_folder+"/"+run))
                    #print(file_name.replace(".dat","_ntuple.root"))
                    print(f"Will process {file_name}")
                    to_process = file_name
                    break
            if to_process != None:
                break
        
        
        if to_process == None:
            print("Nothing to process")
        else:
            #First, copy file localy
            #print(f"scp daquser@muedaq03:/data/muedaq/{run}/{to_process} {daq_folder}")
            os.system(f"scp daquser@muedaq03:/data/muedaq/{run}/{to_process} {daq_folder}")
            unpack(daq_folder+to_process, run_number)
            ntuple_file = get_ntuple_file(to_process,run_number)
            #Getting list of valid ntuple files:
            fList_raw = glob.glob(f"{unpacked_folder}/{run_number}/*.root")
            
            hadd_cmd = f"hadd -f {unpack_merged_folder}/{run_number}.root"
            for f in fList_raw:
                if os.path.getsize(f) > 1000:
                    hadd_cmd+=" "+f+" "
            
            # os.system(f"hadd -f {unpack_merged_folder}/{run_number}.root )
            os.system(hadd_cmd)
            plot(f"{unpack_merged_folder}/{run_number}.root")
            plot_file = get_plot_file(f"{unpack_merged_folder}/{run_number}.root")
            print(plot_file)
            build_web(plot_file)
            
            #Remove raw file
            print(f"Removing {daq_folder}/{to_process}")
            os.system(f"rm {daq_folder}/{to_process}")
        #if reprocess == False:
        #    fList = [f for f in fList if time.time() - os.path.getmtime(f) < reprocess_time  or not os.path.isfile(get_ntuple_file(f))]
        
    elif stage == "dqm":
        #First step : getting list of files to process :
        fList = glob.glob(unpacked_folder+"*")
        if reprocess == False:
            fList = [f for f in fList if not os.path.isfile(get_plot_file(f)) ]
        
        print(f"Will process {len(fList)} files !")
        for ntuple_file in fList:
            if os.path.getsize(ntuple_file) > 1000:
                plot(ntuple_file)
                plot_file = get_plot_file(ntuple_file)
                build_web(plot_file)
            else:
                print("Ignoring file < 1kb")

    elif stage == "plot":
        fList = glob.glob(plots_folder+"*")
        if reprocess == False:
            fList = [f for f in fList if not os.path.isdir(get_web_folder(f)) ]
        
        print(f"{datetime.now().strftime('%y/%m/%d %H:%M:%S')} : Will process {len(fList)} files !")
        for plot_file in fList:
            if os.path.getsize(plot_file) > 1000:
                build_web(plot_file)
            else:
                print("Ignoring file < 1kb")
    stop_time = datetime.now().replace(microsecond=0)
    print(f"DONE!")
    print(f"Time : {stop_time.strftime('%H:%M:%S')}")
    print(f"Run time : {stop_time-start_time}")
    time.sleep(1)
os.system(f"rm {lock_file}")
