from app import app
from flask import render_template, request
import subprocess
from datetime import datetime, timedelta
import os,sys,math, glob, json
from magnet_db import event, magnet_db
from dummy_db import dummy_db

config_file = "/home/xtaldaq/Env_monitoring/magnet-test-env-monitoring/conf_batch.json"

files_dir = "/home/xtaldaq/Env_monitoring/magnet-test-env-monitoring/"

psu_naming ={
             "LV1":"V1 (KIT)",
             "LV2":"V2 (BRU)",
             "LV3":"V3 (PMT)",
             "HV1":"BRU",
             "HV2":"KIT",
             "LI1":"I1 (KIT)",
             "LI2":"I2 (BRU)",
             "LI3":"I3 (PMT)",
             "LV4":"N/A",
             "LI4":"N/A",
             "HV1":"BRU",
             "HV2":"KIT",
             "HI1":"BRU",
             "HI2":"KIT"
             }

def time_select_blocks(start,stop):
    now = datetime.now()
    yesterday = now +timedelta(hours=-2)
    months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    data = {}
    for block,tt in [("start",yesterday),("stop",now)]:
        data[block] = f"<select id='{block}_year'><option>2021</option><option>2020</option></select>"
        data[block]+= f"<select id='{block}_month'>"
        for i in range(12):
            if i+1 == tt.month:
                data[block]+=f"<option value='{i+1}' selected>{months[i]}</option>"
            else:
                data[block]+=f"<option value='{i+1}'>{months[i]}</option>"

        data[block]+=f"</select><select id='{block}_day'>"
        for i in range(31):
            if i+1 == tt.day:
                data[block]+=f"<option value='{i+1}' selected>{i+1:02d}</option>"
            else:
                data[block]+=f"<option value='{i+1}' >{i+1:02d}</option>"

        data[block]+=f"</select><select id='{block}_hour'>"
        for i in range(24):
            if i == tt.hour:
                data[block]+=f"<option value='{i}' selected>{i:02d}h</option>"
            else:
                data[block]+=f"<option value='{i}' >{i:02d}h</option>"
        data[block]+=f"</select><select id='{block}_min'>"
        for i in range(60):
            if i == tt.minute:
                data[block]+=f"<option value='{i}' selected>{i:02d}</option>"
            else:
                data[block]+=f"<option value='{i}'>{i:02d}</option>"
        data[block]+="</select>"
    return data





@app.route('/')
@app.route('/index')

def index():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    fList = sorted(glob.glob(files_dir+'*.bin'), key=os.path.getmtime)
    fList.reverse()
    files = [f.split("/")[-1] for f in fList]
    return render_template('plots.html',start_block = data["start"], stop_block=data["stop"], file_list = files)

@app.route('/psu')
def psu():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    #fList = sorted(glob.glob(files_dir+'*.bin'), key=os.path.getmtime)
    #fList.reverse()
    #files = [f.split("/")[-1] for f in fList]
    return render_template('plots_psu.html',start_block = data["start"], stop_block=data["stop"], file_list = [])

@app.route('/plotting_data')
def plotting_data():
    conf = json.load(open(config_file,"r"))
    debug_message = ""
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)
    data_file = files_dir+request.args.get('data_file')
    data = {"time":[], "temp" : {}, "hum" : {}, "rate" :{}}

    db = magnet_db(config_file,data_file)

    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < stop:
            entry_number_stop = to_check
    debug_message+=f"Start dt : {db.get_entry(entry_number_start)['timestamp']-start} <br/>"
    debug_message+=f"Stop  dt : {db.get_entry(entry_number_stop)['timestamp']-stop} <br/>"

    #print (debug_message)

    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        if not entry==None:
            for key in entry.data:
                val = entry.data[key]

                if key == "timestamp":
                    data["time"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))
                if not key in conf["meas_to_show"]:
                    continue
                data_type = "None"
                if  "_T" in key or "PT_" in key:
                    data_type = "temp"
                elif  "_H" in key:
                    data_type = "hum"
                elif "Rate" in key:
                    data_type = "rate"
                if data_type == "None":
                    print(data_type)
                    continue

                if key in conf["alias"].keys():
                    key = conf["alias"][key]


                if not key in data[data_type]:
                    data[data_type][key] = [val]
                else:
                    if data_type == "temp" and val == -50.0:
                        data[data_type][key].append(data[data_type][key][-1])
                    else:
                        data[data_type][key].append(val)

        entry_number += read_every

    #creating plots from data :
    plots = {"temp" : [], "hum" : [], "rate" : []}
    for meas_type in ["temp","hum", "rate"]:
        for meas in data[meas_type]:
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["time"],
                "type" : "scatter",
                "name" : meas
            })

    return plots

def val_converter(meas_type,val):
    if "lv" in meas_type or "hv_v" in meas_type:
        return 0.001*val
    if "hv_i" in meas_type:
        return 1e-9*val


@app.route('/plotting_data_psu')
def plotting_data_psu():
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)

    #data_file = files_dir+request.args.get('data_file')
    data_file = "/home/xtaldaq/Env_monitoring/psu_monitoring/psu.db"
    data = {"date":[], "lv_v" : {}, "lv_i" : {}, "hv_v" : {}, "hv_i" : {}}

    db = dummy_db(data_file)

    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < stop:
            entry_number_stop = to_check


    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        if not entry==None:
            for key in entry:
                val = entry[key]

                if key == "date":
                    data["date"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))

                data_type = "None"
                if  "LV" in key:
                    data_type = "lv_v"
                elif  "LI" in key:
                    data_type = "lv_i"
                elif  "HV" in key:
                    data_type = "hv_v"
                elif  "HI" in key:
                    data_type = "hv_i"
                if data_type == "None":
                    continue

                if not key in data[data_type]:
                    data[data_type][key] = [val_converter(data_type,val)]
                else:
                    data[data_type][key].append(val_converter(data_type,val))

        entry_number += read_every

    #creating plots from data :
    plots = {"lv_v" : [], "lv_i" : [],"hv_v" : [], "hv_i" : []}
    for meas_type in ["lv_v","lv_i","hv_v","hv_i"]:
        for meas in data[meas_type]:
            name = meas
            if meas in psu_naming.keys():
                name = psu_naming[meas]
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["date"],
                "type" : "scatter",
                "name" : name
            })
    return plots

#/home/xtaldaq/MagnetTest/DQM/ph2_mt_dqm/python/web_server/app/static/WEB_PAGE/Run_61i
@app.route('/dqm')
@app.route('/static/WEB_PAGE/')
def dqm():
    fList = [[f.split("/")[-1], datetime.fromtimestamp(os.path.getmtime(f)).strftime("%Y-%m-%d %H:%M:%S")] for f in sorted(glob.glob("/afs/cern.ch/user/d/delcourt/work/MUONE_DQM/web_page/run*"))]
    fList.reverse()
    return render_template('dqm.html',run_list=fList)

@app.route('/spill_viewer/')
def spill_viewer():
    return render_template('spill_viewer.html')


@app.route('/plotting_data_live')
def plotting_data_live():
    data_file = files_dir+"/default.bin"
    db = magnet_db(config_file,data_file)
    n_points = request.args.get('n_points')
    if not n_points == None and n_points.isdigit():
        n_points = int(n_points)
    else:
        n_points = 5*60
    data = {"date" : [], "rate" : []}
    entry_number = -1*n_points
    while(entry_number < 0):
        
        entry = db.get_entry(entry_number)
        data["date"].append(datetime.fromtimestamp(entry["timestamp"]).strftime("%Y-%m-%d %H:%M:%S"))
        data["rate"].append(entry["Rate_2"])
        entry_number += 1
    
    #Computing spill count :
    min_hits = 8
    spills = []
    in_spill = False
    spill_count = 0
    for count in data["rate"]:
        if in_spill:
            if count < min_hits:
                spills.append(spill_count)
                spill_count = 0
                in_spill = False
            else:
                spill_count+=count
        else:
            if count > min_hits:
                in_spill = True
                spill_count = count
    if spill_count > 0:
        spills.append(spill_count)
    spills.reverse()
    data["spills"] = spills
    return data