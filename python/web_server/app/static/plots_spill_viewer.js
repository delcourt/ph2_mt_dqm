var T0 = 273.5;
var C  = 2.216679;
var a  = 611.6441;
var m  = 7.591386;

function get_abs(h,T){
  return(C*1.0/(T+T0)*a*Math.pow(10,(m*T*1./(T+T0)))*h*0.01);
 }

function get_graph(dt = 0){
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
          var data = JSON.parse(xmlhttp.responseText);
            
          var layout_rates = {
            title: {
              text:'Scintillator rates',
            },
            yaxis: {
              title: {
                text: 'Hz',
              }
            }
          };

          var plot_rates ={
            "y" : data["rate"],
            "x" : data["date"],
            "type" : "scatter",
            "name" : "Scintillator"
          };
          //if(document.getElementById('plot_rate').innerHTML == "") {
            Plotly.newPlot('plot_rate', [plot_rates],layout_rates);
          /*}
          else{
            Plotly.update('plot_rate', [plot_rates],layout_rates)
          }*/
          //alert(data["spills"]);
          var latest_spills_txt = ""
          for (var i = 0; i < data["spills"].length; i++){
            latest_spills_txt+=data["spills"][i] + "  <br/>";
          }
          document.getElementById('latest_spills').innerHTML=latest_spills_txt;
        }
        
    }
    xmlhttp.open("GET", "/plotting_data_live" , false );
    xmlhttp.send(null);
}

document.getElementById('plot_rate').innerHTML = "";
get_graph();
window.setInterval(get_graph,1000);
