export HOSTNAME=`hostname` 

export BASE_DIR="/afs/cern.ch/user/d/delcourt/work/MUONE_DQM/ph2_mt_dqm/python/web_server";
export SERVER_IP="188.185.125.64";
cd $BASE_DIR;

export FLASK_APP=main.py
export FLASK_ENV=development
export FLASK_DEBUG=1
export PYTHONPATH=$BASE_DIR/services:$BASE_DIR/app:$PYTHONPATH
/usr/local/bin/flask run -h $SERVER_IP -p 80
