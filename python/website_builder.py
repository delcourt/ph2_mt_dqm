import glob

base_plot_str = """<img class='wleftzoom' width='168' height='168' src='./small/{file_name}.png' alt='A little explanation here always helps' onclick="popupDiv('./full/{file_name}', 700, 500 , 'A little explanation here always helps','pdf');" />"""



class website_builder:
    def __init__(self,directory, index_template = "index_template.html"):
        self.directory = directory
        with open(index_template,"r") as f:
            self.full_template = f.read()
        self.plot_block = ""


    def generate(self):
        plotList = glob.glob(self.directory+"/small/*.png")
        for full_plot_name in plotList:
            plot_name = full_plot_name.split("/")[-1].replace(".png","")
            self.add_plot(plot_name)
        self.write_index()

    def add_plot(self,plot_name):
        self.plot_block += base_plot_str.format(file_name = plot_name)

    def write_index(self):
        run_name = self.directory.split("/")[-1]
        DQM = self.directory + "/"+run_name+".root"
        text = self.full_template.format(DQM=DQM,RUN_NAME = run_name, PLOTS = self.plot_block)
        with open(self.directory+"/index.html","w") as f:
            f.write(text)
