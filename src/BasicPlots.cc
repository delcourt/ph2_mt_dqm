#include "BasicPlots.h"
#include <numeric>
#include <algorithm>
using namespace std;


void add_all_basic_plots(Histogrammer * hm){
    add_header_plots(hm);
    add_beam_profile(hm);
}
/*
    _tt->SetBranchAddress("L1A"        ,&(e.L1A));
    _tt->SetBranchAddress("error"      ,&(e.Error));
    _tt->SetBranchAddress("TDC"        ,&(e.TDC));
*/

struct Equals
{
    const float x;
    Equals(float y) : x(y) {}
    bool operator()(float y) const { return y == x; }
};

float L1A_prod(const event & e){return e.L1A;}
void  Cbc_L1A_prod(const event & e, TH1 * h){for (auto ll : *(e.CbcL1A)){h->Fill(ll);}}
float Cbc_L1A_mismatch_prod(const event & e){return !all_of(e.CbcL1A->begin(), e.CbcL1A->end(), Equals(*(e.CbcL1A->begin())));}
void  Cbc_Pipe_prod(const event & e, TH1 * h){for (auto ll : *(e.Pipeline)){h->Fill(ll);}}
float Cbc_Pipe_mismatch_prod(const event & e){return !all_of(e.Pipeline->begin(), e.Pipeline->end(), Equals(*(e.Pipeline->begin())));}
void  Cbc_Error(const event & e, TH1 * h){for (auto ll : *(e.Error)){h->Fill(ll);}}
float Cbc_Error_avg(const event & e){return accumulate(e.Error->begin(),e.Error->end(),0)*1./e.Error->size();}



void add_header_plots(Histogrammer * hm){
    hm->Add_1d_hist(L1A_prod,"L1A", "L1A value", "#events",512,0,512);
    hm->Add_1d_hist(Cbc_L1A_prod,"Cbc_L1A", "Cbc L1A value", "#events",512,0,512);
    hm->Add_1d_hist(Cbc_L1A_mismatch_prod,"Cbc_L1A_mismatch", "Has a L1A mismatch", "#events",2,0,1);
    hm->Add_time_hist(Cbc_L1A_mismatch_prod,"Cbc_L1A_mismatch_time","time","# events w/ L1A mismatches", false);
    hm->Add_time_hist(Cbc_Pipe_mismatch_prod,"Pipeline_mismatch_time","time","# events w/ pipeline mismatches",false);
    hm->Add_1d_hist(Cbc_Pipe_mismatch_prod,"Pipeline_mismatch","Has a pipeline mismatch","#events",2,0,1);
    hm->Add_1d_hist(Cbc_Error,"CBC_error","Cbc error word", "#CBC",4,0,4);
    hm->Add_time_hist(Cbc_Error_avg,"CBC_error_time","time","Avg CBC error");
}


void  S0_prod(const event & e, TH1 * h){for (auto hh : *(e.hit0)){h->Fill(hh);}}
void  S1_prod(const event & e, TH1 * h){for (auto hh : *(e.hit1)){h->Fill(hh);}}
float n_hits_0_prod(const event & e){return e.hit0->size();}
float n_hits_1_prod(const event & e){return e.hit1->size();}
float n_hits_prod(const event & e)  {return e.hit1->size() + e.hit0->size();}
void  Stubs_prod(const event & e, TH1 * h){for (auto ss : *(e.stub)){h->Fill(ss);}}
void  Stub_bend_prod(const event & e, TH1 * h){for (auto ss : *(e.bendCode)){h->Fill(ss);}}
float n_stubs_prod(const event & e){return e.stub->size();}

float   Stub_latency_checker_prod(const event & e){ 
	bool should_have_stub_CBC0 = (e.hit0->size() > 0) && e.hit0->at(0) < 127 &&(e.hit1->size() > 0) && e.hit1->at(0) < 127;
 	bool         has_stub_CBC0 = e.stub->size() > 0 && e.stub->at(0) < 254;
	return should_have_stub_CBC0 + 2*has_stub_CBC0;
}

void add_beam_profile(Histogrammer * hm){
    hm->Add_1d_hist(S0_prod,"Hits_sensor_0","Strip #","#Hits");
    hm->Add_1d_hist(S1_prod,"Hits_sensor_1","Strip #","#Hits");
    hm->Add_1d_hist(n_hits_0_prod,"hit_occ_S0","#Hits on S0","#Events");
    hm->Add_1d_hist(n_hits_1_prod,"hit_occ_S1","#Hits on S1","#Events");
    hm->Add_1d_hist(n_hits_prod,"hit_occ_sum","Total # hits","#Events",2*4096,0,2*4096);
    hm->Add_time_hist(n_hits_0_prod,"hit_occ_S0_time","#hits on S0 /event","time",true);
    hm->Add_time_hist(n_hits_1_prod,"hit_occ_S1_time","#hits on S1 /event","time",true);
    hm->Add_time_hist(n_hits_prod,"hit_occ_sum_time","#hits/event","time",true);
    hm->Add_1d_hist(Stubs_prod,"Stubs","Stub position","#Stubs",4096,0,4096);
    hm->Add_1d_hist(Stub_bend_prod,"Stubs_bend","Stub bend","#Stubs",32,0,32);
    hm->Add_1d_hist(n_stubs_prod,"N_stubs","#Stubs","#Events",48,0,48);
    hm->Add_time_hist(n_stubs_prod,"N_stubs_time","#Stubs","time");
    hm->Add_1d_hist(Stub_latency_checker_prod,"Stub_checker","[!S; !R] - [!S; R] - [S; !R] - [S; R]","#Events",4,0,4);
}



