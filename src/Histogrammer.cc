#include "Histogrammer.h"
#include "TH1F.h"
#include "TGraph.h"
using namespace std;

Histogrammer::Histogrammer(){}
Histogrammer::~Histogrammer(){
    for (auto & entry : _1d_hists)
        delete entry.first;
    
    for (auto & entry : _2d_hists)
        delete entry.first;
    
}


void Histogrammer::Add_1d_hist(float function (const event & e), std::string hist_title, string legendX, string legendY, int nbinx, int x0, int x1){
    TH1 * hist = new TH1F(hist_title.c_str(),hist_title.c_str(),nbinx, x0, x1);
    hist->GetXaxis()->SetTitle(legendX.c_str());
    hist->GetYaxis()->SetTitle(legendY.c_str());
    _1d_hists.push_back(std::pair <TH1 *,float (*)(const event &)> (hist,function));
}

void Histogrammer::Add_1d_hist(float function (const event & e), std::string hist_title, int nbinx, int x0, int x1){
    this->Add_1d_hist(function,hist_title, "","", nbinx, x0, x1);
}


void Histogrammer::Add_2d_hist(std::pair<float,float> function (const event & e),string hist_title, string legendX, string legendY, int nbinx, int x0 , int x1, int nbiny, int y0, int y1){
    TH2 * hist = new TH2F(hist_title.c_str(),hist_title.c_str(),nbinx, x0, x1, nbiny, y0, y1);
    hist->GetXaxis()->SetTitle(legendX.c_str());
    hist->GetYaxis()->SetTitle(legendY.c_str());
    _2d_hists.push_back(std::pair <TH2 *, std::pair<float,float> (*) (const event & e)> (hist,function));
}

void Histogrammer::Add_2d_hist(std::pair<float,float> function (const event & e), std::string hist_title, int nbinx, int x0 , int x1, int nbiny, int y0, int y1){
    this->Add_2d_hist(function, hist_title,"", "", nbinx, x0, x1, nbiny, y0, y1);
}


void Histogrammer::Add_1d_hist(void function (const event & e, TH1 * h), std::string hist_title, string legendX, string legendY, int nbinx, int x0, int x1){
    TH1 * hist = new TH1F(hist_title.c_str(),hist_title.c_str(),nbinx, x0, x1);
    hist->GetXaxis()->SetTitle(legendX.c_str());
    hist->GetYaxis()->SetTitle(legendY.c_str());
    _1d_hists_manual.push_back(std::pair <TH1 *,void (*)(const event &, TH1 * h)> (hist,function));
}

void Histogrammer::Add_1d_hist(void function (const event & e, TH1 * h), std::string hist_title, int nbinx, int x0, int x1){
    this->Add_1d_hist(function,  hist_title, "", "" , nbinx, x0, x1);
}


void Histogrammer::Add_2d_hist(void function (const event & e, TH2 * h), std::string hist_title, string legendX, string legendY,  int nbinx, int x0 , int x1, int nbiny, int y0, int y1){
    TH2 * hist = new TH2F(hist_title.c_str(),hist_title.c_str(),nbinx, x0, x1, nbiny, y0, y1);
    hist->GetXaxis()->SetTitle(legendX.c_str());
    hist->GetYaxis()->SetTitle(legendY.c_str());
    _2d_hists_manual.push_back(std::pair <TH2 *, void (*) (const event & e, TH2 * h)> (hist,function));
}

void Histogrammer::Add_2d_hist(void function (const event & e, TH2 * h), std::string hist_title, int nbinx, int x0 , int x1, int nbiny, int y0, int y1){
    this->Add_2d_hist(function,hist_title, "", "" ,nbinx,x0,x1,nbiny,y0,y1);
}

void Histogrammer::Add_time_hist(float function (const event & e), string hist_title, string legendX, string legendY, bool average){
    std::map <uint32_t, std::pair<float, float>> data_container;
    std::vector<string> titles;
    titles.push_back(hist_title);
    titles.push_back(legendX);
    titles.push_back(legendY);
    std::tuple <vector<string>, float (*) (const event & e),bool> hist_definition(titles,function,average);
    _time_hists.push_back(std::pair <std::map <uint32_t, std::pair<float, float>> , std::tuple < vector<string>, float (*) (const event & e), bool>>(data_container,hist_definition));
}

void Histogrammer::Add_time_hist(float function (const event & e), std::string hist_title, bool average){
    this->Add_time_hist(function,hist_title, "", "", average);
}



void Histogrammer::Fill_Hists(const event & e){
    for (auto & entry : _1d_hists)
        entry.first->Fill((entry.second)(e));

    for (auto & entry : _1d_hists_manual)
        (entry.second)(e,entry.first);

    for (auto & entry : _2d_hists_manual)
        (entry.second)(e,entry.first);

    for (auto & entry : _2d_hists){
        std::pair<float,float> results = (entry.second)(e);
        entry.first->Fill(results.first,results.second);
    }

    for (auto & entry : _time_hists){
        float results = get<1>(entry.second)(e);
        entry.first[e.Timestamp].first  += results;
        entry.first[e.Timestamp].second += get<2>(entry.second);
    }
}

void Histogrammer::Save_Hists(std::string fName){
    TFile * storage_file = new TFile(fName.c_str(),"RECREATE");
    for (auto & entry: _1d_hists)
        entry.first->Write();
    for (auto & entry: _2d_hists)
        entry.first->Write();
    for (auto & entry: _1d_hists_manual)
        entry.first->Write();
    for (auto & entry: _2d_hists_manual)
        entry.first->Write();


    for (auto & entry: _time_hists){
        std::vector<float> values;
        std::vector<float> times;
        uint32_t t0 = 0;
        for(auto & data_point : entry.first){
            float denominator = data_point.second.second;
            if (denominator == 0)
                denominator = 1;
            values.push_back(data_point.second.first *1./denominator);
            if (t0 == 0){
                t0 = data_point.first;
            }
            times.push_back(data_point.first-t0);
        }

        TGraph * g = new TGraph(values.size(),&times[0], &values[0]);
	vector<string> title = get<0>(entry.second);
        g->SetTitle(title.at(0).c_str());
	if (title.size() > 1){
            g->GetXaxis()->SetTitle(title.at(1).c_str());
	}
	if (title.size() > 2){
            g->GetYaxis()->SetTitle(title.at(2).c_str());
	}
        g->Write(title.at(0).c_str());
        delete g;
    }

    storage_file->Close();    
}

void Histogrammer::Process_ntuple(std::string ntuple_name, std::string plots_name){
    TreeReader r(ntuple_name);
    int n_entries = r.getEntries();
    cout<<"Processing ntuple. N entries = "<<n_entries<<endl;
    
    int event = 0;
    while(r.getEvent()){
        Fill_Hists(r.e);
        if (event%10000 == 0)
            cout<<"Processing event "<<event<<"/"<<n_entries<<endl;
        event++;
    }
    Save_Hists(plots_name);
    cout<<"Done !"<<endl;
}
