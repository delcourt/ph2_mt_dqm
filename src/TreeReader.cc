#include "TreeReader.h"
using namespace std;


event::event():hit0(new vector <int>),hit1(new vector <int>),stub(new vector <int>),
               bendCode(new vector <int>),bend(new vector <float>),stubSim(new vector <int>),
               bendCodeSim(new vector <int>),bendSim(new vector <int>),CbcL1A(new vector <uint16_t>),
               Pipeline(new vector <uint16_t>),Error(new vector <uint16_t>)
               {}

event::~event(){
    delete hit0;
    delete hit1;
    delete stub;
    delete bendCode;
    delete bend; 
    delete stubSim; 
    delete bendCodeSim;
    delete bendSim; 
    delete CbcL1A;
    delete Pipeline;
    delete Error;
}


TreeReader::TreeReader(string fName_){
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("flatTree");
    event_counter = 0;
    n_entries = _tt->GetEntries();
    _tt->SetBranchAddress("hit0"       ,&(e.hit0));
    _tt->SetBranchAddress("hit1"       ,&(e.hit1));
    _tt->SetBranchAddress("stub"       ,&(e.stub));
    _tt->SetBranchAddress("bendCode"   ,&(e.bendCode));
    _tt->SetBranchAddress("bend"       ,&(e.bend));
    _tt->SetBranchAddress("stubSim"    ,&(e.stubSim));
    _tt->SetBranchAddress("bendCodeSim",&(e.bendCodeSim));
    _tt->SetBranchAddress("bendSim"    ,&(e.bendSim));
    _tt->SetBranchAddress("L1A"        ,&(e.L1A));
    _tt->SetBranchAddress("CbcL1A"     ,&(e.CbcL1A));
    _tt->SetBranchAddress("pipeline"   ,&(e.Pipeline));
    _tt->SetBranchAddress("error"      ,&(e.Error));
    _tt->SetBranchAddress("TDC"        ,&(e.TDC));
    _tt->SetBranchAddress("Timestamp"  ,&(e.Timestamp));
    _tt->SetBranchAddress("delay_and_group",&(e.delay_and_group));
    _tt->SetBranchAddress("vcth"       ,&(e.vcth));


}
TreeReader::~TreeReader(){
    _ff->Close();
    delete _ff;
}

int TreeReader::getEvent(int event_number){
    if (event_number == -1){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if (event_number < n_entries){
            _tt->GetEntry(event_number);
            return 1;
        }
        else{
            return 0;
        }
    }
}

int TreeReader::getEntries(){
    return n_entries;
}
